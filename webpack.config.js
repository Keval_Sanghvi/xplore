const currentTask = process.env.npm_lifecycle_event;
const path = require("path");
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
let cssConfig = {
    test: /\.s[ac]ss$/i,
    use: [
            "css-loader",
        {
            loader: "sass-loader",
            options: {
                implementation: require("sass")
            }
            }
        ]
};

let config = {
    entry: "./app/assets/scripts/app.js",
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './app/index.html',
        })
    ],
    module: {
        rules: [
            cssConfig
        ],
    }
};

if (currentTask == "dev") {
    config.output = {
        filename: 'app.bundled.js',
        path: path.resolve(__dirname, 'app')
    };

    config.devServer = {
        before: function (app, server) {
            server._watch("./app/**/*.html")
        },
        contentBase: path.resolve(__dirname, "app"),
        port: 5000,
        hot: true,
        host: "0.0.0.0"
    };

    config.mode = 'development';
    cssConfig.use.unshift('style-loader');
}

if (currentTask == "build") {
    config.mode = "production";
    config.output = {
        filename: '[name].[chunkhash].bundled.js',
        path: path.resolve(__dirname, 'dist')
    };
    config.optimization = {
        splitChunks: {
            chunks: 'all'
        }
    };

    config.plugins.push(
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: 'styles.[chunkhash].css'
        })
    );

    cssConfig.use.unshift(MiniCssExtractPlugin.loader);
}
module.exports = config;
