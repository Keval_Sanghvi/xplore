import $ from "jquery"

function clear() {
    $("#clear").removeClass("hide").addClass("flex");
    $(".clear").click(() => {
        $("#clear").addClass("hide").removeClass("flex");
        $("#search").val("");
        deleteSearchResults();
        clearResultsLine();
    });
    $(".clear").keydown((e) => {
        if (e.key === "Enter" || e.key === " ") {
            e.preventDefault();
            $("#clear").click();
        }
    });
}

function update(result) {
    clearResultsLine();
    deleteSearchResults();
    if (result.length) {
        buildSearchResults(result);
        setResultsLine(result.length);
    }
}

function deleteSearchResults() {
    const parentElement = $("#searchResults")[0];
    while (parentElement.childNodes.length) {
        parentElement.removeChild(parentElement.firstChild);
    }
};

function buildSearchResults(resultArray) {
    resultArray.forEach((result) => {
        const resultItem = createResultItem(result);
        const resultContents = document.createElement("div");
        resultContents.classList.add("resultContents");
        if (result.img) {
            const resultImage = createResultImage(result);
            resultContents.append(resultImage);
        }
        const resultText = createResultText(result);
        resultContents.append(resultText);
        resultItem.append(resultContents);
        const searchResults = $("#searchResults")[0];
        searchResults.append(resultItem);
    });
};

function createResultItem(result) {
    const resultItem = document.createElement("div");
    resultItem.classList.add("resultItem");
    const resultTitle = document.createElement("div");
    resultTitle.classList.add("resultTitle");
    const link = document.createElement("a");
    link.href = `https://en.wikipedia.org/?curid=${result.id}`;
    link.textContent = result.title;
    link.target = "_blank";
    resultTitle.append(link);
    resultItem.append(resultTitle);
    return resultItem;
};

function createResultImage(result) {
    const resultImage = document.createElement("div");
    resultImage.classList.add("resultImage");
    const img = document.createElement("img");
    img.src = result.img;
    img.alt = result.title;
    resultImage.append(img);
    return resultImage;
};

function createResultText(result) {
    const resultText = document.createElement("div");
    resultText.classList.add("resultText");
    const resultDescription = document.createElement("p");
    resultDescription.classList.add("resultDescription");
    resultDescription.textContent = result.text;
    resultText.append(resultDescription);
    return resultText;
};

function clearResultsLine() {
    $("#result").text("");
};

function setResultsLine(totalResults) {
    if (totalResults) {
        $("#result").text(`Displaying ${totalResults} results.`);
    } else {
        $("#result").text("Sorry, no results.");
    }
};

export {
    clear,
    update
};
