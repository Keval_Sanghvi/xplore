import $ from "jquery";

async function start() {
    // Get search term from input
    let searchTerm = $("#search").val().trim().replace(/  +/g, ' ');

    // Get maxChars as per width of the window
    const width = window.innerWidth || document.body.clientWidth;
    let maxChars;
    if (width < 414) {
        maxChars = 90;
    } else if (width < 1400) {
        maxChars = 120;
    } else if (width >= 1400) {
        maxChars = 150;
    }

    // Get data from wikipedia API
    const url = `https://en.wikipedia.org/w/api.php?action=query&generator=search&gsrsearch=${searchTerm}&gsrlimit=25&prop=pageimages|extracts&exchars=${maxChars}&exintro&explaintext&exlimit=max&format=json&origin=*`;
    const response = await fetch(url);
    if (!response.ok) {
        await Promise.reject(new Error(response.status));
    }
    const data = await response.json();
    return data;
}

export {
    start
};
