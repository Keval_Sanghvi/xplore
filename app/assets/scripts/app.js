import "../styles/styles.scss";
import $ from 'jquery';
import {
    start
} from "./modules/getData";
import {
    clear,
    update
} from "./modules/ui";

let result = [];

$("#searchBar").submit(async (e) => {
    e.preventDefault();
    clear();
    let response = await start();
    if (response.hasOwnProperty("query")) {
        processResults(response.query.pages);
    }
    update(result);
});

function processResults(response) {
    result = [];
    Object.keys(response).forEach((key) => {
        const id = key;
        const title = response[key].title;
        const text = response[key].extract;
        const img = response[key].hasOwnProperty("thumbnail") ?
            response[key].thumbnail.source :
            null;
        const item = {
            id,
            title,
            img,
            text
        };
        result.push(item);
    });
}

if (module.hot) {
    module.hot.accept();
}
